<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Koduliising\Liisi\Model;

use \Magento\Framework\UrlInterface;


/**
 * Pay In Store payment method model
 */
class LiisiPayment extends \Magento\Payment\Model\Method\AbstractMethod
{
	// Status codes
	const SERVICE_1011 = 1011;
    const SERVICE_1012 = 1012;

    const RESPONSE_1111 = 1111;
    const RESPONSE_1911 = 1911;

	// Custom fields
	public $service = "1012";
	public $currency;
	public $language = "EST";
    public $mode = "test";
	public $charset = "UTF-8";
	public $version = "008";
	public $snd;

	public $public_key;
	public $private_key;
	public $private_key_pass;

	public $API_URL = array("test"=>'https://prelive.liisi.ee/api/ipizza/', "live"=>'https://klient.liisi.ee/api/ipizza/');


    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'liisipayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    public function getLiisiAPI()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Koduliising\Liisi\Helper\Data');

        $this->mode = ($helper->getGeneralConfig('test_mode') ? "test" : "live");
        $this->snd = $helper->getGeneralConfig('snd_id');
        $this->private_key = $helper->getGeneralConfig('private_key');
        $this->private_key_pass = $helper->getGeneralConfig('private_key_pass');
        $this->public_key = $helper->getGeneralConfig('public_key');

    }


    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    protected function padding($string)
    {
        if (!empty($this->charset)) {
            return str_pad(mb_strlen($string, $this->charset), 3, '0', STR_PAD_LEFT);
        } else {
            return str_pad(strlen($string), 3, '0', STR_PAD_LEFT);
        }
    }

    public function getFormFields($order, $return_url)
    {
        date_default_timezone_set("Europe/Tallinn");
        $this->getLiisiAPI();
        $form_values = array();
        $form_values['VK_SERVICE'] = $this->service;
        $form_values['VK_VERSION'] = $this->version;
        $form_values['VK_SND_ID'] = $this->snd;

        $form_values['VK_STAMP'] = $order->getIncrementId();
        $form_values['VK_AMOUNT'] = round($order->getGrandTotal(), 2);
        $form_values['VK_CURR'] = $order->getOrderCurrencyCode();
        $form_values['VK_REF'] = $order->getReference();
        $form_values['VK_MSG'] = "Tellimus ".$order->getIncrementId();
        $form_values['VK_RETURN'] = rtrim($return_url, '/');
        $form_values['VK_CANCEL'] = rtrim($return_url, '/');
        $form_values['VK_DATETIME'] = date('Y-m-d\TH:i:sO');

        $form_values['VK_MAC'] = $this->generateMAC($form_values);
        $form_values['VK_ENCODING'] = $this->charset;
        $form_values['VK_LANG'] = $this->language;

        return $form_values;
    }

    public function getUrl()
    {
        return ($this->mode == "test" ? $this->API_URL['test'] : $this->API_URL['live']);
    }

    protected function generateMAC($values)
    {
        try {
            $data = array();
            foreach ($values as $value) {
                $data[] = $this->padding($value) . $value;
            }

            $private = openssl_get_privatekey(
                $this->private_key,
                $this->private_key_pass
            );
            if (!$private) {
                //throw new Exception('Invalid liisi private key');
            }

            openssl_sign(implode('', $data), $signature, $private);
            $mac = base64_encode($signature);
            openssl_free_key($private);
        } catch (Exception $e) {
            return "";
        }

        return $mac;
    }

    protected function extractRequestData(array $request)
    {
        $data = array();
        $fields = array(
            'VK_SERVICE',
            'VK_VERSION',
            'VK_SND_ID',
            'VK_REC_ID',
            'VK_STAMP',
        );

        $fields_1111 = array(
            'VK_T_NO',
            'VK_AMOUNT',
            'VK_CURR',
            'VK_REC_ACC',
            'VK_REC_NAME',
            'VK_SND_ACC',
            'VK_SND_NAME',
            'VK_REF',
            'VK_MSG',
            'VK_T_DATETIME',
        );
        $fields_1911 = array('VK_REF', 'VK_MSG');

        if (isset($request['VK_SERVICE']) &&
            in_array($request['VK_SERVICE'], array(self::RESPONSE_1911, self::RESPONSE_1111))
        ) {
            $fields = array_merge($fields, ${'fields_'.$request['VK_SERVICE']});
        } else {
            return false;
        }

        foreach ($fields as $field) {
            if (isset($request[$field])) {
                $data[$field] = $this->padding($request[$field]).$request[$field];
            } else {
                return false;
            }
        }

        return $data;
    }

    protected function extraRequestSign(array $request)
    {
        if (isset($request['VK_MAC'])) {
            return $request['VK_MAC'];
        }
    }

    public function verifySign(array $request)
    {
        $this->getLiisiAPI();
        try {
            $signature = $this->extraRequestSign($request);
            $data = $this->extractRequestData($request);
        } catch (Exception $e) {
            return false;
        }
        if ($data == false) return false;

        $signature = base64_decode($signature);
        $public_key = openssl_get_publickey($this->public_key);
        $out = openssl_verify(implode('', $data), $signature, $public_key);
        openssl_free_key($public_key);

        return $out;
    }
}
