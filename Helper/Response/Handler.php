<?php

namespace Koduliising\Liisi\Helper\Response;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Phrase;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Koduliising\Liisi\Model\LiisiPaymentFactory;

class Handler
{
    const AUTO_REQUEST_PARAM_NAME = 'VK_AUTO';
    const AUTO_REQUEST_PARAM_VALUE = 'Y';
    const ORDER_ID_PARAM_NAME = 'VK_STAMP';
    const REQUEST_STATUS_PARAM_NAME = 'VK_SERVICE';

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var LiisiPaymentFactory
     */
    private $liisiPaymentFactory;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Construct function.
     *
     * @param LiisiPaymentFactory $liisiPaymentFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderFactory $orderFactory
     * @param InvoiceService $invoiceService
     * @param InvoiceRepositoryInterface $invoiceRepository
     */
    public function __construct(
        LiisiPaymentFactory $liisiPaymentFactory,
        OrderRepositoryInterface $orderRepository,
        OrderFactory $orderFactory,
        InvoiceService $invoiceService,
        InvoiceRepositoryInterface $invoiceRepository
    ) {
        $this->liisiPaymentFactory = $liisiPaymentFactory;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param RequestInterface $request
     *
     * @return int
     */
    public function getRequestType(RequestInterface $request)
    {
        return (int)$request->getParam(static::REQUEST_STATUS_PARAM_NAME);
    }

    /**
     * @param RequestInterface $request
     *
     * @return bool
     */
    public function isAutoRequest(RequestInterface $request)
    {
        return $request->getParam(static::AUTO_REQUEST_PARAM_NAME) === static::AUTO_REQUEST_PARAM_VALUE;
    }

    /**
     * @param RequestInterface $request
     *
     * @return Order
     * @throws NotFoundException
     */
    public function loadOrderFromRequest(RequestInterface $request)
    {
        $order = $this->orderFactory->create()->loadByIncrementId($request->getParam(
            static::ORDER_ID_PARAM_NAME
        ));

        if ($order->getId() === null) {
            throw new NotFoundException(new Phrase('Order is not found.'));
        }

        return $order;
    }

    /**
     * @param RequestInterface $request
     *
     * @return OrderInterface|Order
     * @throws NotFoundException
     */
    public function setOrderAsCancelled(RequestInterface $request)
    {
            $order = $this->loadOrderFromRequest($request);
            $order->cancel();
            $order->addStatusHistoryComment(__('Customer has cancelled payment'));
            $order->save();

    }

    /**
     * @param RequestInterface $request
     *
     * @return OrderInterface|Order
     * @throws NotFoundException
     * @throws LocalizedException
     */
    public function setOrderAsProcessing(RequestInterface $request)
    {
        $order = $this->loadOrderFromRequest($request);
        $invoice = $this->invoiceService->prepareInvoice($order);

        $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
        $this->invoiceRepository->save($invoice->register());

        return $this->orderRepository->save(
            $order
                ->setState(Order::STATE_PROCESSING)
                ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING))
        );
    }

    /**
     * @param RequestInterface $request
     *
     * @return bool
     */
    public function verifyRequest(RequestInterface $request)
    {
        return $this->liisiPaymentFactory->create()->verifySign($request->getParams());
    }
}
